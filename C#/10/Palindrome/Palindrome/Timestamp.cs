﻿namespace Palindrome
{
    public class Timestamp
    {

        /// <summary>
        /// Checks the amount of palindrome occurrences in a given range of lower and upper timestamps, inclusive.
        /// </summary>
        /// <param name="h1">lower hour</param>
        /// <param name="m1">lower minute</param>
        /// <param name="s1">lower second</param>
        /// <param name="h2">upper hour</param>
        /// <param name="m2">upper minutes</param>
        /// <param name="s2">upper seconds</param>
        /// <returns>The amount of palindrome occurrences in a given range.</returns>
        public static int Palindrome(int h1, int m1, int s1, int h2, int m2, int s2)
        {
            string lower = $"{h1:D2}:{m1:D2}:{s1:D2}";
            string current = lower;
            string upper = $"{h2:D2}:{m2:D2}:{s2:D2}";
            int occurences = 0;

            if (CheckReversible(lower))
            {
                occurences++;
            }

            bool done = false;
            while (!done)
            {
                current = Tick(current);
                if (CheckReversible(current))
                {
                    occurences++;
                }

                if (current.Equals(upper))
                {
                    done = true;
                }
            }
            return occurences;
        }

        /// <summary>
        /// Ticks a given timestring one second.
        /// </summary>
        /// <param name="timeStr">The timestring on the format "HH:MM:SS" to tick one second.</param>
        /// <returns>The given timestring ticked one second, on the format "HH:MM:SS".</returns>
        private static string Tick(string timeStr)
        {
            string[] digits = timeStr.Split(':');
            int s = int.Parse(digits[2]);
            int m = int.Parse(digits[1]);
            int h = int.Parse(digits[0]);

            s++;
            s = s % 60;

            if (s == 0)
            {
                m++;
                m = m % 60;
                if (m == 0)
                {
                    h++;
                    h = h % 24;
                }
            }
            return $"{h:D2}:{m:D2}:{s:D2}";
        }

        /// <summary>
        /// Checks whether a string is reversible. EG. lol is reversible since it is equal read both forwards and backwards.
        /// </summary>
        /// <param name="str">The string to check if is reversible</param>
        /// <returns></returns>
        private static bool CheckReversible(string str)
        {
            bool result = false;
            string reverse = Reverse(str);
            if (str.Equals(reverse))
            {
                result = true;
            }
            return result;
        }

        /// <summary>
        /// Reverses a given string.
        /// </summary>
        /// <param name="s">The string to reverse.</param>
        /// <returns>The given string reversed.</returns>
        private static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }
}
