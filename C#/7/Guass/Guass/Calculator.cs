﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Guass
{
    public class Calculator
    {

        /// <summary>
        /// Calculates the Guass sum up to a given number.
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public int Guass(int n)
        {
            return (int)((n / 2.0) * (n + 1));
        }

        /// <summary>
        /// Calculates the Guass sum of a given range.
        /// </summary>
        /// <param name="n"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public int Guass(int n, int m)
        {
            return (int)(((m + ((n - m) & ((n - m) >> (sizeof(int) * 8 - 1)))) +
                (n - ((n - m) & ((n - m) >> (sizeof(int) * 8 - 1))))) *
                (n - ((n - m) & ((n - m) >> (sizeof(int) * 8 - 1))) -
                (m + ((n - m) & ((n - m) >> (sizeof(int) * 8 - 1)))) +
                1) / 2.0);
        }
    }
}
