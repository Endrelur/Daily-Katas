﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CelciusConverter
{
    public class Thermostat
    {
        public string Convert(string temperature)
        {
            string err = "Error";
            string[] substr = temperature.Split('*');


            if (substr.Length != 2)
            {
                return err;
            }

            double num = 0.0;
            try
            {
                num = Double.Parse(substr[0]);
            }
            catch (Exception) { return "Error"; }

            switch (substr[1])
            {
                case "C":
                    return ConvertToF(num);
                case "F":
                    return ConvertToC(num);
                default:
                    return err;
            }
        }

        private string ConvertToC(double farenheit)
        {
            int c = (int)((farenheit - 32) / (1.8));
            return $"{c}*C";
        }

        private string ConvertToF(double celcius)
        {
            int f = (int)((celcius * 1.8) + 32);
            return $"{f}*F";
        }
    }
}
