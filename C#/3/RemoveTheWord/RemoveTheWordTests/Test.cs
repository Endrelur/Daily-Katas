using RemoveTheWord;

namespace RemoveTheWordTests
{

    public class Test
    {
        [Fact]
        public void RemoveTheWord_StringAndArray_W()
        {
            char[] testArray = "stringw".ToCharArray();
            string testWord = "string";
            char[] expected = { 'w' };

            char[] acutal = Remover.Do(testArray, testWord);

            Assert.Equal(expected, acutal);
        }

        [Fact]
        public void RemoveTheWord_StringAndArray_BGW()
        {
            char[] testArray = "bbllgnoaw".ToCharArray();
            string testWord = "balloon";
            char[] expected = { 'b', 'g', 'w' };

            char[] acutal = Remover.Do(testArray, testWord);

            Assert.Equal(expected, acutal);
        }

        [Fact]
        public void RemoveTheWord_StringAndArray_Empty()
        {
            char[] testArray = "anryow".ToCharArray();
            string testWord = "norway";
            char[] expected = { };

            char[] acutal = Remover.Do(testArray, testWord);

            Assert.Equal(expected, acutal);
        }

        [Fact]
        public void RemoveTheWord_StringAndArray_TU()
        {
            char[] testArray = "ttestu".ToCharArray();
            string testWord = "testing";
            char[] expected = { 't', 'u' };

            char[] acutal = Remover.Do(testArray, testWord);

            Assert.Equal(expected, acutal);
        }
    }
}