﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoveTheWord
{
    public static class Remover
    {

        public static char[] Do(char[] removeFrom, string remove)
        {
            char[] removeChars = remove.ToCharArray();

            string result = "";

            foreach (char c in removeFrom)
            {
                if (remove.Contains(c))
                {
                    int removeAmount = countCharOccurances(removeChars, c);
                    int removeFromAmount = countCharOccurances(removeFrom, c);
                    if (removeFromAmount > removeAmount)
                    {
                        int resultAmount = countCharOccurances(result.ToArray(), c);
                        if (resultAmount < (removeFromAmount - removeAmount))
                            result += c;
                    }

                }
                else
                {
                    result += c;
                }

            }

            return result.ToCharArray();
        }

        private static int countCharOccurances(char[] array, char letter)
        {
            int i = 0;

            foreach (char c in array)
            {
                if (c == letter)
                {
                    i++;
                }
            }
            return i;
        }
    }
}
