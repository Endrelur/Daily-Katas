﻿using FruitSmoothie.Ingredients;
using FruitSmoothie.Ingredients.milk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FruitSmoothie
{
    public class Smoothie
    {
        public List<IIngredient> Ingredients { get; set; }

        public Smoothie(List<IIngredient> ingredients)
        {
            Ingredients = ingredients;
        }

        public double getCost()
        {
            double totalprice = .0;
            foreach (IIngredient i in Ingredients)
            {
                totalprice += i.price();
            }
            return totalprice;
        }

        public double getPrice()
        {
            return getCost() + getCost() * 1.5;
        }

        public string getName()
        {
            List<string> ingredientNames = new List<string>();

            string milk = "";

            foreach (IIngredient i in Ingredients)
            {
                if (i is IMilk)
                {
                    milk = i.getName();
                }
                else
                {
                    ingredientNames.Add(i.getName());
                }
            }

            ingredientNames.Sort(StringComparer.InvariantCultureIgnoreCase);

            string type = "Smoothie";
            if (ingredientNames.Count > 1)
            {
                type = "Fusion";
            }

            StringBuilder sb = new StringBuilder();

            foreach (string ingredientName in ingredientNames)
            {
                if(!sb.ToString().Contains(ingredientName))
                    sb.Append(ingredientName).Append(" ");
            }
            sb.Append(type);
            if (milk.Length > 0)
            {
                sb.Append(" ").Append(milk);
            }

            return sb.ToString().Trim();
        }

    }
}
