﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FruitSmoothie.Ingredients.fruit
{
    public class Blueberries : IIngredient
    {
        public string getName()
        {
            return "Blueberry";
        }

        public double price()
        {
            return 1.00;
        }
    }
}
