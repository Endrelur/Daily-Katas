﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FruitSmoothie.Ingredients.fruit
{
    public class Raspberries : IIngredient
    {
        public string getName()
        {
            return "Raspberry";
        }

        public double price()
        {
            return 1.00;
        }
    }
}
