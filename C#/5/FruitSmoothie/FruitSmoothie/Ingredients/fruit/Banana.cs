﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FruitSmoothie.Ingredients.fruit
{
    public class Banana : IIngredient
    {
        public string getName()
        {
            return "Banana";
        }

        public double price()
        {
            return 0.50;
        }
    }
}
