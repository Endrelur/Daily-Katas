﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FruitSmoothie.Ingredients.fruit
{
    public class Apple : IIngredient
    {
        public string getName()
        {
            return "Apple";
        }

        public double price()
        {
            return 1.75;
        }
    }
}
