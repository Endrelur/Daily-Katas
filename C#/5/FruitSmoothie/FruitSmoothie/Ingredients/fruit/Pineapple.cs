﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FruitSmoothie.Ingredients.fruit
{
    public class Pineapple : IIngredient
    {
        public string getName()
        {
            return "Pineapple";
        }

        public double price()
        {
            return 3.50;
        }
    }
}
