﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FruitSmoothie.Ingredients.fruit
{
    public class Mango : IIngredient
    {
        public string getName()
        {
            return "Mango";
        }

        public double price()
        {
            return 2.50;
        }
    }
}
