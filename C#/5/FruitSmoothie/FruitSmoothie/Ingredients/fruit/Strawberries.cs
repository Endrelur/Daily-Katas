﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FruitSmoothie.Ingredients.fruit
{
    public class Strawberries : IIngredient
    {
        public string getName()
        {
            return "Strawberry";
        }

        public double price()
        {
            return 1.50;
        }
    }
}
