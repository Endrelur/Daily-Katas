﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FruitSmoothie.Ingredients
{
    public interface IIngredient
    {
        public double price();
        public string getName();
    }
}
