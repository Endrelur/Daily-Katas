﻿using FruitSmoothie;
using FruitSmoothie.Ingredients;
using FruitSmoothie.Ingredients.fruit;
using FruitSmoothie.Ingredients.milk;

Smoothie s = new Smoothie(new List<IIngredient>()
{
    new Banana(),
    new SoyMilk()
});

Console.WriteLine($"{s.getName()}, Cost: {s.getCost()}, Price: {s.getPrice()}");

s = new Smoothie(new List<IIngredient>()
{
    new Raspberries(),
    new Strawberries(),
    new Blueberries(),
    new CowsMilk(),
});

Console.WriteLine($"{s.getName()}, Cost: {s.getCost()}, Price: {s.getPrice()}");

s = new Smoothie(new List<IIngredient>()
{
    new Blueberries(),
    new Banana(),
    new OatMilk(),
});

Console.WriteLine($"{s.getName()}, Cost: {s.getCost()}, Price: {s.getPrice()}");