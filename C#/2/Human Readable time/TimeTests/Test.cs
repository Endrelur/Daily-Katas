using Human_Readable_time;

namespace TimeTests
{
    public class Test
    {
        [Fact]
        public void GetTime_ZeroSeconds_ExpectedStringEqualsActual()
        {
            int time = 0;
            string expected = "00:00:00";

            string actual = Time.GetTimeString(time);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetTime_FiveSeconds_ExpectedStringEqualsActual()
        {
            int time = 5;
            string expected = "00:00:05";

            string actual = Time.GetTimeString(time);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetTime_OneMinute_ExpectedStringEqualsActual()
        {
            int time = 60;
            string expected = "00:01:00";

            string actual = Time.GetTimeString(time);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetTime_OneDay_ExpectedStringEqualsActual()
        {
            int time = 86399;
            string expected = "23:59:59";

            string actual = Time.GetTimeString(time);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetTime_MaxTime_ExpectedStringEqualsActual()
        {
            int time = 359999;
            string expected = "99:59:59";

            string actual = Time.GetTimeString(time);

            Assert.Equal(expected, actual);
        }
    }

}