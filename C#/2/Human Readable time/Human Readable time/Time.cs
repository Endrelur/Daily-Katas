﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Human_Readable_time
{
    public static class Time
    {

        public static string GetTimeString(int seconds)
        {
            if (seconds < 0)
            {
                throw new ArgumentOutOfRangeException("cant have negative time");
            }
            if (seconds > 359999)
            {
                throw new ArgumentOutOfRangeException("Too high");
            }

            int hours = GetHours(seconds);
            int minutes = GetMinutes(seconds - hours * 3600);
            seconds -= hours * 3600 + minutes * 60;

            return $"{hours:D2}:{minutes:D2}:{seconds:D2}";

        }

        private static int GetHours(int seconds)
        {
            if (seconds >= 3600)
            {
                return seconds / 3600;
            }
            else
            {
                return 0;
            }
        }

        private static int GetMinutes(int seconds)
        {
            if (seconds >= 60)
            {
                return seconds / 60;
            }
            else
            {
                return 0;
            }
        }

    }
}
