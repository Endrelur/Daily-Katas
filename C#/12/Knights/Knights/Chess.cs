﻿namespace Knights
{
    public class Chess
    {

        public static bool CannotCapture(int[,] chessBoard)
        {
            bool result = true;

            //x-axis
            for (int x = 0; x < 8 && jh; x++)
            {
                //y-axis
                for (int y = 0; y < 8 && result; y++)
                {
                    if (chessBoard[y, x] == 1)
                    {
                        bool upRight = true;
                        bool upLeft = true;
                        bool downRight = true;
                        bool downLeft = true;
                        bool leftUp = true;
                        bool leftDown = true;
                        bool rightUp = true;
                        bool rightDown = true;
                        if (y > 1)
                        {
                            upRight = checkIndex(chessBoard, y, x, (int)Directions.UpRight);
                            upLeft = checkIndex(chessBoard, y, x, (int)Directions.UpLeft);
                        }
                        if (y < 6)
                        {
                            downRight = checkIndex(chessBoard, y, x, (int)Directions.DownRight);
                            downLeft = checkIndex(chessBoard, y, x, (int)Directions.DownLeft);
                        }
                        if (x > 1)
                        {
                            leftUp = checkIndex(chessBoard, y, x, (int)Directions.LeftUp);
                            leftDown = checkIndex(chessBoard, y, x, (int)Directions.LeftDown);
                        }
                        if (x < 6)
                        {
                            rightUp = checkIndex(chessBoard, y, x, (int)Directions.RightUp);
                            rightDown = checkIndex(chessBoard, y, x, (int)Directions.RightDown);
                        }
                        result = upRight && upLeft && downRight && downLeft && leftUp && leftDown && rightUp && rightDown;
                    }

                }
            }
            return result;
        }



        private static bool checkIndex(int[,] board, int xIndex, int yIndex, int direction)
        {
            int xOffset = 0;
            int yOffset = 0;
            bool result = false;
            switch (direction)
            {
                case (int)Directions.UpRight:
                    xOffset += 1;
                    yOffset -= 2;
                    break;
                case (int)Directions.UpLeft:
                    xOffset -= 1;
                    yOffset -= 2;
                    break;
                case (int)Directions.DownLeft:
                    yOffset += 2;
                    xOffset -= 1;
                    break;
                case (int)Directions.DownRight:
                    yOffset += 2;
                    xOffset += 1;
                    break;
                case (int)Directions.LeftUp:
                    yOffset -= 1;
                    xOffset -= 2;
                    break;
                case (int)Directions.LeftDown:
                    yOffset += 1;
                    xOffset -= 2;
                    break;
                case (int)Directions.RightDown:
                    yOffset += 1;
                    xOffset += 2;
                    break;
                case (int)Directions.RightUp:
                    yOffset -= 1;
                    xOffset += 2;
                    break;
            }

            int checkXIndex = (xIndex + xOffset);
            int checkYIndex = (yIndex + yOffset);

            if (xOffset == 0 || yOffset == 0)
            {
                throw new Exception($"xOffset:{xOffset} , yOffset:{yOffset}");
            }

            if (checkXIndex < 0 || checkYIndex < 0)
            {
                result = true;
            }
            if (checkYIndex > 7 || checkXIndex > 7)
            {
                result = true;
            }
            if (!result)
            {
                result = board[checkXIndex, checkYIndex] != 1;
            }
            if (!result)
            {
                Console.WriteLine($"False with x={xIndex}, {yIndex}, collides with {checkXIndex},{checkYIndex}");
            }
            return result;
        }


        private enum Directions
        {
            UpRight = 1,
            UpLeft = 2,
            DownLeft = 3,
            DownRight = 4,
            LeftUp = 5,
            LeftDown = 6,
            RightUp = 7,
            RightDown = 8,
        }

    }
}
