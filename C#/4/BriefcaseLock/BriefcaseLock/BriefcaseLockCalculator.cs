﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BriefcaseLock
{
    public class BriefcaseLockCalculator
    {
        public int MinimumTurns(int currentLock, int targetLock)
        {
            int[] current = GetDigitsArray(currentLock);
            int[] target = GetDigitsArray(targetLock);
            int result = 0;

            for (int i = 0; i < current.Length; i++)
            {
                result += CountTurns(current[i], target[i]);
            }
            return result;

        }


        private int CountTurns(int from, int to)
        {
            if (from == to)
                return 0;

            int backwards;
            int forwards;
            if (to > from)
            {
                backwards = from - to + 10;
                forwards = to - from;
            }
            else
            {
                backwards = to - from + 10;
                forwards = from - to;
            }

            return Math.Min(forwards, backwards);

        }

        private int[] GetDigitsArray(int num)
        {
            char[] chars = num.ToString("D4").ToCharArray();
            int[] digits = new int[chars.Length];
            for (int i = 0; i < chars.Length; i++)
            {
                digits[i] = Int32.Parse("" + chars[i]);
            }
            return digits;
        }
    }
}
