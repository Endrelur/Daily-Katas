﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PigEnglishTranslator
{
    internal class Translator
    {
        readonly List<char> Consonats = new() { 'b', 'c', 'd', 'f', 'g', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 's', 't', 'v', 'x', 'z', 'h', 'r', 'w', 'y' };


        public string TranslateSentence(string sentenceToTranslate)
        {
            string[] words = sentenceToTranslate.Split(' ');

            for (int i = 0; i < words.Length; i++)
            {
                words[i] = TranslateWord(words[i]);
            }
            StringBuilder st = new StringBuilder();
            foreach (string word in words)
            {
                st.Append(word).Append(" ");
            }
            return st.ToString().TrimEnd();
        }
        public string TranslateWord(string wordToTranslate)
        {
            if (wordToTranslate.Equals(""))
            {
                return wordToTranslate;
            }
            LinkedList<char> word = new LinkedList<char>(wordToTranslate.Trim());

            string suffix = "yay";

            if (Consonats.Contains(char.ToLower(word.FirstOrDefault())) || !char.IsPunctuation(word.LastOrDefault()))
                suffix = "ay";
            else
                return new string(word.ToArray()) + suffix;

            bool consonantsFound = false;
            List<char> consonants = new List<char>();
            while (!consonantsFound)
            {
                char current = word.FirstOrDefault();
                if (Consonats.Contains(char.ToLower(current)))
                {
                    consonants.Add(current);
                    word.RemoveFirst();
                }
                else
                {
                    consonantsFound = true;
                }
            }

            bool specialCharactersFound = false;
            LinkedList<char> punctuation = new LinkedList<char>();
            while (!specialCharactersFound)
            {
                char current = word.LastOrDefault();
                if (char.IsPunctuation(current))
                {
                    punctuation.AddFirst(current);
                    word.RemoveLast();
                }
                else
                {
                    specialCharactersFound = true;
                }
            }

            foreach (char c in consonants)
            {
                word.AddLast(c);
            }
            foreach (char c in suffix)
            {
                word.AddLast(c);
            }
            foreach (char c in punctuation)
            {
                word.AddLast((c));
            }
            return new string(word.ToArray());
        }

    }
}
