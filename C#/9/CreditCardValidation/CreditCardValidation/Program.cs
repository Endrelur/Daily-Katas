﻿using CreditCardValidation;

Validator validator = new Validator();

string card1 = "1234567890123456";
string card2 = "709092739800713";
bool res1 = validator.ValidateCard(card1);
bool res2 = validator.ValidateCard(card2);
Console.WriteLine(res1);
Console.WriteLine(res2);