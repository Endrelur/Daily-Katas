﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditCardValidation
{
    public class Validator
    {
        public bool ValidateCard(string cardNumber)
        {
            LinkedList<int> digits = new LinkedList<int>();
            bool result = false;

            try
            {
                char[] intChars = cardNumber.ToCharArray();
                foreach (char c in intChars)
                {
                    digits.AddLast(c - '0');
                }
            }
            catch (Exception e)
            {
                return result;
            }

            if (digits.Count < 14 || digits.Count > 19)
            {
                return false;
            }

            int checkDigit = digits.LastOrDefault();
            digits.RemoveLast();
            int[] digits2 = digits.Reverse().ToArray();

            for (int i = 0; i < digits2.Length; i += 2)
            {
                digits2[i] = DoubleOperator(digits2[i]);
            }

            int digSum = digits2.Sum();
            int lastDig = digSum % (10);

            if ((10 - lastDig) == checkDigit)
            {
                result = true;
            }
            return result;
        }

        private int DoubleOperator(int x)
        {
            int result = x * 2;
            if (result > 9)
                result = result - 9;
            return result;
        }
    }
}
