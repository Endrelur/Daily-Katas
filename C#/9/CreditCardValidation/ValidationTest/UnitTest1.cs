using CreditCardValidation;

namespace ValidationTest
{
    public class UnitTest1
    {
        string num1 = "79927398714";
        string num2 = "79927398713";
        string num3 = "709092739800713";
        string num4 = "1234567890123456";
        string num5 = "12345678901237";
        string num6 = "5496683867445267";
        string num7 = "4508793361140566";
        string num8 = "376785877526048";
        string num9 = "36717601781975";
        Validator validator = new Validator();

        [Fact]
        public void Test1()
        {
            Assert.Equal(false, validator.ValidateCard(num1));
        }

        [Fact]
        public void Test2()
        {
            Assert.Equal(false, validator.ValidateCard(num2));
        }
        [Fact]
        public void Test3()
        {
            bool res = validator.ValidateCard(num3);
            Assert.Equal(true, res);
        }
        [Fact]
        public void Test4()
        {
            Assert.Equal(false, validator.ValidateCard(num4));
        }
        [Fact]
        public void Test5()
        {
            Assert.Equal(true, validator.ValidateCard(num5));
        }
        [Fact]
        public void Test6()
        {
            Assert.Equal(true, validator.ValidateCard(num6));
        }
        [Fact]
        public void Test7()
        {
            Assert.Equal(false, validator.ValidateCard(num7));
        }
        [Fact]
        public void Test8()
        {
            bool result = validator.ValidateCard(num8);

            Assert.Equal(true, result);
        }
        [Fact]
        public void Test9()
        {
            Assert.Equal(false, validator.ValidateCard(num9));
        }

    }
}