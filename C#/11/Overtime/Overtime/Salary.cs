﻿namespace Overtime
{
    public class Salary
    {
        public static string Overtime(double start, double end, double rate, double multiplier) => "$"+string.Format("{0:0.00}",(((17 - start) > 0.0 ? (17 - start) * rate : 0.0) + ((end - 17) > 0 ? (end - 17) * rate * multiplier : -((17 - end) * rate))));
    }
}
