using Overtime;

namespace OvertimeTests
{
    public class Tests
    {
        [Fact]
        public void Salary_TestIfCorrectResult_ResultIs240()
        {
            string expected = "$240,00";

            string actual = Salary.Overtime(9, 17, 30, 1.5);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Salary_TestIfCorrectResult_ResultIs84()
        {
            string expected = "$84,00";

            string actual = Salary.Overtime(16, 18, 30, 1.8);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Salary_TestIfCorrectResult_ResultIs52point50()
        {
            string expected = "$52,50";

            string actual = Salary.Overtime(13.25, 15, 30, 1.5);

            Assert.Equal(expected, actual);
        }
    }
}