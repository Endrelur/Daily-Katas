﻿using System.Diagnostics;
namespace Parseltounge
{
    public class Parseltounge
    {

        private bool CheckParseltoungeWord(string input)
        {
            char[] charArr = input.ToArray();
            bool done = false;
            bool result = false;
            int index = 0;

            while (!done && index < charArr.Length)
            {
                char c = charArr[index];
                if (c == 's')
                {
                    done = true;
                    int nextIndex = index + 1;
                    if (nextIndex < charArr.Length)
                    {
                        char c2 = charArr[nextIndex];
                        if (c2 == 's')
                        {
                            result = true;
                        }
                    }
                }
                index++;
            }
            return result;
        }

        public bool CheckParseltoungeSentence(string sentence)
        {
            sentence = sentence.ToLowerInvariant();
            if (!sentence.Contains("s"))
            {
                return true;
            }

            string[] words = sentence.Split(' ');
            bool done = false;
            bool result = false;
            int index = 0;

            while (!done && index < words.Length)
            {
                string word = words[index];
                if (word.Contains('s'))
                {
                    result = CheckParseltoungeWord(word);
                    if (result == false)
                    {
                        done = true;
                    }
                }
                index++;
            }
            return result;
        }
    }

}







