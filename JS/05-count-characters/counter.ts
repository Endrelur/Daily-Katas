/*
export default function charCount(character: string, sentence: string) {
  if (character.length !== 1) {
    return "error";
  }

  let counter: number = 0;

  for (let c = 0; c < sentence.length; c++) {
    if (sentence[c] === character) {
      counter++;
    }
  }
  return counter
  ;
}
*/
export default function charCount(
  character: string,
  sentence: string,
  index: number = 0
) {
  if (character.length != 1) {
    return "error";
  } else {
    let result = 0;
    if (index < sentence.length) {
      if (sentence[index] === character) {
        result++;
      }
      result += charCount(character, sentence, index + 1);
    } else {
      if (sentence[index] === character) {
        result++;
      }
    }
    return result;
  }
}
