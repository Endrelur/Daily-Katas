import { expect, it } from "@jest/globals";
import charCount from "./counter";

it("should be equal to 1", () => {
  const expected = 1;

  const actual = charCount("a", "edabit");

  expect(actual).toBe(expected);
});

it("should be equal to 1", () => {
  const expected = 1;

  const actual = charCount("c", "Chamber of secrets");

  expect(actual).toBe(expected);
});

it("should be equal to 0", () => {
  const expected = 0;

  const actual = charCount("B", "boxes are fun");

  expect(actual).toBe(expected);
});

it("should be equal to 4", () => {
  const expected = 4;

  const actual = charCount("b", "big fat bubble");

  expect(actual).toBe(expected);
});

it("should be equal to 0", () => {
  const expected = 0;

  const actual = charCount("e", "javascript is good");

  expect(actual).toBe(expected);
});

it("should be equal to 2", () => {
  const expected = 2;

  const actual = charCount("!", "!easy!");

  expect(actual).toBe(expected);
});

it("should be error", () => {
  const acutal = charCount("wow", "the universe is wow");
  const expected = "error";

  expect(acutal).toBe(expected);
});
