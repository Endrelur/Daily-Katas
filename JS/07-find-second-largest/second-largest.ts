export default function secondLargest(arr: number[]) {
  return arr.length != 0
    ? arr.length != 1
      ? arr.length == 2 && arr[0] === arr[1]
        ? 0
        : arr.sort((a, b) => b - a)[1]
      : arr.sort((a, b) => b - a)[0]
    : 0;
}
