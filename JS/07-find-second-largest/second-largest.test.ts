import { expect, jest, test, it } from "@jest/globals";
import secondLargest from "./second-largest";

it("should be 40", () => {
  const expected = 40;
  const arr = [10, 40, 30, 20, 50];

  const actual = secondLargest(arr);

  expect(actual).toBe(expected);
});

it("should be 105", () => {
  const expected = 105;
  const arr = [25, 143, 89, 13, 105];

  const actual = secondLargest(arr);

  expect(actual).toBe(expected);
});

it("should be 23", () => {
  const expected = 23;
  const arr = [54, 23, 11, 17, 10];

  const actual = secondLargest(arr);

  expect(actual).toBe(expected);
});

it("should be 0", () => {
  const expected = 0;
  const arr = [1, 1];

  const actual = secondLargest(arr);

  expect(actual).toBe(expected);
});

it("should be 1", () => {
  const expected = 1;
  const arr = [1];

  const actual = secondLargest(arr);

  expect(actual).toBe(expected);
});

it("should be 0", () => {
  const expected = 0;
  const arr = [];

  const actual = secondLargest(arr);

  expect(actual).toBe(expected);
});
