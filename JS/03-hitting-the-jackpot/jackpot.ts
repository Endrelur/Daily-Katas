export default function testJackpot(input: any[]) {
  let result = true;
  if (input.length != 4) {
    return false;
  }
  for (let i = 1; i < input.length && result; i++) {
    result = Object.is(JSON.stringify(input[i]), JSON.stringify(input[0]));
  }
  return result;
}
