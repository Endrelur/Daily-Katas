import { expect, it } from "@jest/globals";
import testJackpot from "./jackpot";

it("should be jackpot", () => {
  const arr = ["@", "@", "@", "@"];

  const expected = true;

  let actual = testJackpot(arr);

  expect(actual).toBe(expected);
});

it("should be jackpot", () => {
  const arr = ["abc", "abc", "abc", "abc"];

  const expected = true;

  let actual = testJackpot(arr);

  expect(actual).toBe(expected);
});

it("should be jackpot", () => {
  const arr = ["SS", "SS", "SS", "SS"];

  const expected = true;

  let actual = testJackpot(arr);

  expect(actual).toBe(expected);
});

it("should not be jackpot", () => {
  const arr = ["&&", "&", "&&&", "&&&&"];

  const expected = false;

  let actual = testJackpot(arr);

  expect(actual).toBe(expected);
});

it("should not be jackpot", () => {
  const arr = ["SS", "SS", "SS", "Ss"];

  const expected = false;

  let actual = testJackpot(arr);

  expect(actual).toBe(expected);
});

it("should not be jackpot", () => {
  const arr = [1, "1", 1, "1"];

  const expected = false;

  let actual = testJackpot(arr);

  expect(actual).toBe(expected);
});

it("should be jackpot", () => {
  const arr = [1, 1, 1, 1];

  const expected = true;

  let actual = testJackpot(arr);

  expect(actual).toBe(expected);
});

it("should not be jackpot", () => {
  let kari = { name: "Kari", type: "menneske" };

  let gunnar = { name: "Gunnar", type: "menneske" };

  let ola = { name: "Ola", type: "menneske" };

  let marte = { name: "Marte", type: "menneske" };

  const arr = [kari, gunnar, ola, marte];

  const expected = false;

  let actual = testJackpot(arr);

  expect(actual).toBe(expected);
});

it("should be jackpot", () => {
  let kari0 = { name: "Kari", type: "menneske" };
  let kari1 = { name: "Kari", type: "menneske" };
  let kari2 = { name: "Kari", type: "menneske" };
  let kari3 = { name: "Kari", type: "menneske" };

  const arr = [kari0, kari1, kari2, kari3];

  const expected = true;

  let actual = testJackpot(arr);

  expect(actual).toBe(expected);
});
