export default function isValidIp(ipStr: string) {
  const splitString = ipStr.split(".");
  let valid: boolean = true;
  if (splitString.length !== 4) {
    valid = false;
  }
  if (splitString[3] === "0") {
    valid = false;
  }

  for (let i = 0; i < splitString.length && valid; i++) {
    if (splitString[i] !== "0") {
      valid =
        valid &&
        checkValidRange(1, 255, splitString[i]) &&
        noLeadingZeros(splitString[i]);
    }
  }

  return valid;
}

function checkValidRange(start: number, end: number, input: string) {
  const inputNum: number = parseInt(input);

  return inputNum >= start && inputNum <= end ? true : false;
}

function noLeadingZeros(input: string): boolean {
  let result: boolean = true;
  if (input.length > 1) {
    input[0] === "0" ? (result = false) : (result = true);
  }
  return result;
}
