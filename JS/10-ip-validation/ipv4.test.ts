import { expect, it } from "@jest/globals";
import isValidIp from "./ipv4";

it("should be valid ip", () => {
  const expected = true;

  const acutal = isValidIp("1.2.3.4");

  expect(acutal).toBe(expected);
});

it("should not be valid ip", () => {
  const expected = false;

  const acutal = isValidIp("1.2.3");

  expect(acutal).toBe(expected);
});

it("should not be valid ip", () => {
  const expected = false;

  const acutal = isValidIp("1.2.3.4.5");

  expect(acutal).toBe(expected);
});

it("should be valid ip", () => {
  const expected = true;

  const acutal = isValidIp("123.45.67.89");

  expect(acutal).toBe(expected);
});

it("should not be valid ip", () => {
  const expected = false;

  const acutal = isValidIp("123.456.78.90");

  expect(acutal).toBe(expected);
});
it("should not be valid ip", () => {
  const expected = false;

  const acutal = isValidIp("123.045.067.089");

  expect(acutal).toBe(expected);
});
