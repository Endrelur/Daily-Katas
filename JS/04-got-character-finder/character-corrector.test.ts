import { expect, it } from "@jest/globals";
import correctTitle from "./character-corrector";

it("should be correct title", () => {
  const input = "jOn SnoW, kINg IN thE noRth";

  const expected = "Jon Snow, King in the North.";

  const actual = correctTitle(input);

  expect(actual).toBe(expected);
});

it("should be correct title", () => {
  const input = "TYRION LANNISTER, HAND OF THE QUEEN.";

  const expected = "Tyrion Lannister, Hand of the Queen.";

  const actual = correctTitle(input);

  expect(actual).toBe(expected);
});

it("should be correct title", () => {
  const input = "sansa stark,lady of winterfell.";

  const expected = "Sansa Stark, Lady of Winterfell.";

  const actual = correctTitle(input);

  expect(actual).toBe(expected);
});
