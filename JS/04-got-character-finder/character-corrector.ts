export default function correctTitle(input: string) {
  const name = input.split(",")[0].toLowerCase().split(" ").filter(Boolean);
  const title: string[] = input
    .split(",")[1]
    .toLowerCase()
    .split(" ")
    .filter(Boolean);
  let result = "";
  name.forEach((piece) => {
    result += appendSpace(upperCaseFirstLetter(piece));
  });
  result = appendSpace(result.trim() + ",");
  result += appendSpace(upperCaseFirstLetter(title[0]));
  for (let i = 1; i < title.length - 1; i++) {
    result += appendSpace(title[i]);
  }
  result += upperCaseFirstLetter(title[title.length - 1]);
  if (!result.endsWith(".")) {
    result += ".";
  }
  return result.trim();
}

function upperCaseFirstLetter(word: string) {
  word = word.toLowerCase();
  return word[0].toUpperCase() + word.slice(1);
}

function appendSpace(word: string) {
  return (word += " ");
}
