export default function grantTheHint(str: string) {
  const words = str.trim().split(" ");
  let result: string[] = [];
  const longest = findLongestWord(str) + 1;
  for (let i = 0; i < longest; i++) {
    result[i] = "";
  }
  for (let i = 0; i < words.length; i++) {
    const currentWord = words[i];
    const blankCurrentWord = blankString(currentWord);
    for (let j = 0; j < longest; j++) {
      let index = j;
      if (!(index < currentWord.length)) {
        index = currentWord.length;
      }
      result[j] +=
        currentWord.substring(0, index) +
        blankCurrentWord.substring(index, blankCurrentWord.length) +
        " ";
    }
  }
  for (let i = 0; i < result.length; i++) {
    result[i] = result[i].trim();
  }
  return result;
}

function blankString(str: string) {
  let result = "";
  for (let i = 0; i < str.length; i++) {
    result += "_";
  }
  return result;
}

function findLongestWord(str: string) {
  var longestWord = str.split(" ").sort(function (a, b) {
    return b.length - a.length;
  });
  return longestWord[0].length;
}
