export default function numGrid(arr: any[][]) {
  for (let x = 0; x < 5; x++) {
    for (let y = 0; y < 5; y++) {
      if (arr[y][x] !== "#") {
        arr[y][x] = countNeighbors(x, y, arr);
      }
    }
  }
  return arr;
}

function countNeighbors(x: number, y: number, arr: any[][]): string {
  const above_x = x;
  const above_y = y - 1;

  const aboveleft_x = x - 1;
  const aboveleft_y = y - 1;

  const aboveright_x = x + 1;
  const aboveright_y = y - 1;

  const downleft_x = x - 1;
  const downleft_y = y + 1;

  const downright_x = x + 1;
  const downright_y = y + 1;

  const under_x = x;
  const under_y = y + 1;

  const right_x = x + 1;
  const right_y = y;

  const left_x = x - 1;
  const left_y = y;

  const coordinates = [
    above_x,
    above_y,
    under_x,
    under_y,
    right_x,
    right_y,
    left_x,
    left_y,
    aboveleft_x,
    aboveleft_y,
    aboveright_x,
    aboveright_y,
    downleft_x,
    downleft_y,
    downright_x,
    downright_y,
  ];

  let count = 0;
  for (let i = 0; i < coordinates.length; i += 2) {
    const x = coordinates[i];
    const y = coordinates[i + 1];
    if (x < 5 && y < 5 && x >= 0 && y >= 0) {
      if (arr[y][x] === "#") {
        count++;
      }
    }
  }
  return "" + count;
}
