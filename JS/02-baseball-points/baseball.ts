export default function points(twoPointers: number, threePointers: number) {
  return twoPointers * 2 + threePointers * 3;
}
