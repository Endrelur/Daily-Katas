import { expect, jest, test, it } from "@jest/globals";
import points from "./baseball";

it("should be 5", () => {
  const expected = 5;

  const actual = points(1, 1);

  expect(actual).toBe(expected);
});
