import { expect, test } from "@jest/globals";
import happyAlgorithm from "./happy";

test("139 should equal HAPPY 5", () => {
  const num = 139;
  const expected = "HAPPY 5";

  const actual = happyAlgorithm(num);

  expect(actual).toBe(expected);
});

test("67 should equal SAD 10", () => {
  const num = 67;
  const expected = "SAD 10";

  const actual = happyAlgorithm(num);

  expect(actual).toBe(expected);
});

test("1 should equal HAPPY 1", () => {
  const num = 1;
  const expected = "HAPPY 1";

  const actual = happyAlgorithm(num);

  expect(actual).toBe(expected);
});

test("89 should equal SAD 8", () => {
  const num = 89;
  const expected = "SAD 8";

  const actual = happyAlgorithm(num);

  expect(actual).toBe(expected);
});
