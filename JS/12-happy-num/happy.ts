export default function happyAlgorithm(num: number, step: number=1, hist: number[]=[num]): string {
  return (num = Array.from(num.toString()).reduce((prev, curr) => prev + (+curr) ** 2,0)) === 1 ? `HAPPY ${step}`: hist.includes(num) ? `SAD ${step}`: happyAlgorithm(num, ++step, [...hist, num]);
}