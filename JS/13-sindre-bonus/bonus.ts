export default function getLength(arr: any[]): number {
  let currentCount = 0;
  arr.forEach((value) => {
    if (Array.isArray(value)) {
      currentCount += getLength(value);
    } else {
      currentCount++;
    }
  });
  return currentCount;
}
