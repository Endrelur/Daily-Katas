import { expect, test } from "@jest/globals";
import getLength from "./bonus";

it("should equal 3", () => {
  const expected = 3;
  const arr = [1, [2, 3]];

  const actual = getLength(arr);

  expect(actual).toBe(expected);
});

it("should equal 4", () => {
  const expected = 4;
  const arr = [1, [2, [3, 4]]];

  const actual = getLength(arr);

  expect(actual).toBe(expected);
});

it("should equal 6", () => {
  const expected = 6;
  const arr = [1, [2, [3, [4, [5, 6]]]]];

  const actual = getLength(arr);

  expect(actual).toBe(expected);
});

it("should equal 5", () => {
  const expected = 5;
  const arr = [1, [2], 1, [2], 1];

  const actual = getLength(arr);

  expect(actual).toBe(expected);
});
